﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int lives;
    public GameObject asteroid;
    public Vector3 spawnValues;
    public int asteroidcount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public GUIText scoretext;
    private int score;
    public GUIText gameOverText;
    public GUIText livesText;

    private bool gameOver;

    void Start()
    {
        gameOver = false;
        score = 0;
        Updatescore();
        StartCoroutine(SpawnWaves());
        Lives();
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < asteroidcount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(asteroid, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            asteroidcount++;
            yield return new WaitForSeconds(waveWait);    
            if(gameOver)
            {
                break;
            }
        }
    }
    void Updatescore()
    {
        scoretext.text = "Score: " + score;
    }
    public void addscore(int newscore)
    {
        score += newscore;
        Updatescore();
    }
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
    public void Lives()
    {
        livesText.text = "Lives: " + lives;
    }
    public void deductlives()
    {
        lives--;
        Lives();
    }
}
