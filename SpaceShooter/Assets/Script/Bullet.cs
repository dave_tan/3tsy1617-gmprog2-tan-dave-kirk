﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float BulletSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Vector3.forward * BulletSpeed * Time.deltaTime);
	}
    void OnTriggerEnter (Collider Other)
    {
        if(Other.tag == "Player")
        {
            return;
        }
        Destroy(Other.gameObject);
        Destroy(gameObject);
        Debug.Log("GameObject " + Other.gameObject.name);
    }
}
