﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {
    public float speed = 0.5f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MeshRenderer mr = GetComponent<MeshRenderer>();
        Material mt = mr.material;
        Vector3 offset = mt.mainTextureOffset;
        offset.y += speed * Time.deltaTime;
        mt.mainTextureOffset = offset;
    }
}
