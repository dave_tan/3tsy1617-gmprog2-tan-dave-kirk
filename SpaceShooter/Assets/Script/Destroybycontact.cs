﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroybycontact : MonoBehaviour
{
    GameObject player;
    public int scorevalue;
    private GameController gameController;

    void Start()
    {
        GameObject gamecontrollerobject = GameObject.FindWithTag("GameController");
        if(gamecontrollerobject!=null)
        {
            gameController = gamecontrollerobject.GetComponent<GameController>();
        }
        if(gameController == null)
        {
            Debug.Log("Cannot Find  'GameController' script");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player player1 = GetComponent<Player>();
            gameController.deductlives();
            if (gameController.lives == 0)
            {
                Destroy(other.gameObject);
                gameController.GameOver();
            }
            
        }
        else
        {
            Destroy(other.gameObject);
        }
        gameController.addscore(scorevalue);
        Destroy(gameObject);
    }
}
