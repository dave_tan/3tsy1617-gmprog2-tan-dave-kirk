﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    public float speed;
    private float scale;
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 targetPos;
        targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        targetPos = new Vector3(targetPos.x, targetPos.y, 0);

        this.transform.position = Vector3.MoveTowards(
                                this.transform.position,
                                targetPos,
                                speed * Time.deltaTime / transform.localScale.x);
    }
}
