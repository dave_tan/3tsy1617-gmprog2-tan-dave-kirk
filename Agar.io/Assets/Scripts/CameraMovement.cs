﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public GameObject player;
    private Vector3 offset;

	// Use this for initialization
	void Start ()
    {
        offset = transform.position - player.transform.position;
        StartCoroutine(movement());
	}
	
	// Update is called once per frame
	IEnumerator movement()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.5f);
            transform.position = player.transform.position + offset;
        }
        
	}
}
