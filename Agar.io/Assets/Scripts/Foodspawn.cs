﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foodspawn : MonoBehaviour {
    public GameObject Food;
    private float foodcount;
	// Use this for initialization
	void Start ()
    {
        foodcount = 30;
        firstfood();
        StartCoroutine(FoodSpawner());
	}
	
	// Update is called once per frame
    IEnumerator FoodSpawner()
    {

        while (foodcount != 101)
        {

            int x = Random.Range(0, Camera.main.pixelWidth);
            int y = Random.Range(0, Camera.main.pixelHeight);

            Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
            target.z = 0;

            foodcount++;
            Instantiate(Food, target, Quaternion.identity);
            yield return new WaitForSeconds(1);
        }
    }
    void firstfood()
    {
        for (int i = 0; i < 30; i++)
        {

            int x = Random.Range(0, Camera.main.pixelWidth);
            int y = Random.Range(0, Camera.main.pixelHeight);

            Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
            target.z = 0;

            Instantiate(Food, target, Quaternion.identity);
        }
    }
}
