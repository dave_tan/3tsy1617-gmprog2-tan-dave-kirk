﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ColorRandomizer : MonoBehaviour {
    public List<Material> Colors = new List<Material>();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Awake()
    {
        GetComponent<Renderer>().material = Colors[Random.Range(0, Colors.Count)];
    }
}
