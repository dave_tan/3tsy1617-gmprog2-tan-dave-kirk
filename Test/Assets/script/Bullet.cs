﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float BulletSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Vector3.up * BulletSpeed * Time.deltaTime);
	}
    void OnTriggerEnter(Collider Other)
    {
        Destroy(Other.gameObject);
        Debug.Log("GameObject " + Other.gameObject.name);
    }
}
